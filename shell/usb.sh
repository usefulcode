#!/bin/sh
pointM=$(cat /proc/partitions|grep ub|tail -n1)
pointM=/dev/${pointM##* }
echo "detecting usb devices...."
mdev -s
test -b ${pointM}
if [ $? -eq 0 ] 
then
	echo "usb disk detected at ${pointM}"
	echo "mounting....."
	mkdir usbdisk -p
	umount usbdisk
	mount ${pointM} usbdisk
	echo "mount disk successfully!"
	echo "files in usb disk is"
	ls usbdisk -lh
	exit 0
else
	echo "could not find usb disk!"
	exit 1
fi
