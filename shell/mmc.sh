#!/bin/sh
message="no thing"
needStop()
{
	test -e /mmc_stop
	if [ $? -eq 0 ]
	then
		echo "YES"
	else
		echo "NO"
	fi
}
waitIN()
{
	while true
	do
		if [ "$(getStatus)" == "IN" ]
		then
			message="Insert Detected"
			break
		fi
		sleep 1
		if [ "$(needStop)" != "NO" ]
		then
			message="Has been interrupted by user!"
			break
		fi
	done
}

waitOUT()
{
	while true
	do
		if [ "$(getStatus)" == "OUT" ]
		then
			message="Unplug Detected"
			break
		fi
		sleep 1
		if [ "$(needStop)" != "NO" ]
		then
			message="Has been interrupted by user!"
			break
		fi
	done
}

getStatus(){
	a=$(cat /proc/partitions|grep mmc|tail -n1)
	test -z "$a"
	if [ $? -eq 0 ] 
	then
		echo "OUT"
	else
		echo "IN"
	fi
}

temp=$(getStatus)
echo "the initial state of the sdcard:${temp}"
if [ "${temp}" == "IN" ]
then
	echo "please unplug the sd card"
	waitOUT
	echo ${message}
	echo "please insert the sd card"
	waitIN
	echo ${message}

else
	echo "please insert the sd card"
	waitIN
	echo ${message}
	echo "please unplug the sd card"
	waitOUT
	echo ${message}
fi
