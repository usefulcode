#!/bin/sh

echo "start Tiger DB HardWare Test=================="
echo "ALSA ->1"
echo "NAND ->2"
echo "LCD  ->3"
echo "RTC  ->4"
echo "TOUCH->5"
echo "USB  ->6"
echo "WIFI ->7"
echo "HDMI ->8"
echo "Ad7997->9"
echo "GPS  ->a"
echo "ALL-TEST  ->0"
echo; echo "Hit a key, then go test."
read Keypress

case "$Keypress" in
	[1]         ) ./alsa.sh;;
	[2]         ) ./nand.sh;;
	[3]         ) ./lcd.sh;;
	[4]         ) ./rtc.sh;;
	[5]         ) ./ts.sh;;
	[6]         ) ./usb.sh;;
	[7]         ) ./wifi.sh;;
	[8]         ) ./hdmi.sh;;
	[9]         ) ./ad7997.sh;;
	[0]         ) ./all.sh;;
	[a]         ) ./gps.sh;;
 *             ) echo "Punctuation, whitespace, or other";;
esac    

exit 0
