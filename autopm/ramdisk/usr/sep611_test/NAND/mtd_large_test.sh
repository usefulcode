#!/bin/sh
#writed by fjj

for val in 3
do
	if [ -d /mnt/nand/flash$val ]
	then
		echo "/mnt/nand/flash$val directory has existed."
	else
		mkdir -p /mnt/nand/flash$val
	fi

	echo "======================================================================"
	echo "We are operating mtd$val"
	echo "flash_eraseall"
	flash_eraseall /dev/mtd$val
	echo "ubiattach"
	ubiattach /dev/ubi_ctrl -m $val -d $val
	echo "mdev for /dev/ubi$val"
	mdev -s
	echo "ubimkvol"
	ubimkvol /dev/ubi$val -s 1024MiB -N ubitest$val

	echo "mount nand"
	time mount -t ubifs ubi$val:ubitest$val /mnt/nand/flash$val
	
	echo "IOzone test"
	./iozone -s 128M -i 0 -i 1 -f /mnt/nand/flash$val/iozone_tmpfile0 -Rab ./iozone0.xls -r 128k
	
	echo "umount nand"
	time umount /mnt/nand/flash$val
done

