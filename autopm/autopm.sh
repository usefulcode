#!/bin/sh
/bin/echo -e "[\x1b[1;33mPermition\x1b[0m] \x1b[1;31mChecking root permition....\x1b[0m"
if test "$(whoami)" != "root"
then
echo "You must be root!" 
echo "Run this shell using sudo or as root!"
exit
fi
SDCARD=$(dmesg |grep removable|tail -n1)
SDCARD=$(echo ${SDCARD##*\[})
SDCARD=$(echo ${SDCARD%\]*})
SDSIZE=$(dmesg|grep ${SDCARD}|grep "GB"|tail -n1|cut -d \( -f2|cut -d \) -f1)
SDCARD=/dev/${SDCARD%\]*}

/bin/echo -e "[\x1b[1;33mSDCARD\x1b[0m] \x1b[1;31mDetecting at ${SDCARD}.....\x1b[0m"
if test -f "${SDCARD}"; 
then
echo "sdcard cannot be detected!"
exit
fi

echo "****************************************"
echo "******  SDCARD detected at       *******"
/bin/echo -e "******      [\x1b[1;33m${SDCARD}\x1b[0m]             *******"
/bin/echo -e "******   \x1b[1;31m${SDSIZE}\x1b[0m       *******"
echo "****************************************"
/bin/echo -e "\x1b[1;31m"
userkey="somethingelse"
while true
do
if ! test ${userkey}
then
read -p "Are you sure for that?(yes/no):" userkey
elif test ${userkey} = "yes";
then
break
elif test ${userkey} = "no";
then
/bin/echo -e "\x1b[0m"
exit
else
read -p "Are you sure for that?(yes/no):" userkey
fi
done
/bin/echo -e "\x1b[0m"
umount ${SDCARD}1 >> /dev/null
umount ${SDCARD}2 >> /dev/null
umount ${SDCARD}3 >> /dev/null
umount ${SDCARD}4 >> /dev/null
echo "****************************************"
echo "******     Fdisk the SD SCARD    *******"
echo "******          SEU              *******"
echo "******     chinawrj@gmail.com    *******"
echo "****************************************"
/bin/echo -e "\x1b[1;31m"
sfdisk ${SDCARD} -x >>/dev/null <<EOF
20,400,L
420,,L


EOF
fdisk -l ${SDCARD}
/bin/echo -e "\x1b[0m"
echo "****************************************"
echo "******     make fs on SD SCARD   *******"
echo "******          SEU              *******"
echo "******     chinawrj@gmail.com    *******"
echo "****************************************"
/bin/echo -e "\x1b[1;31m"
echo "create fat32 on ${SDCARD}1"
/bin/echo -e "\x1b[0m"
mkfs.vfat ${SDCARD}1

/bin/echo -e "\x1b[1;31m"
echo "create ext2 on ${SDCARD}2"
/bin/echo -e "\x1b[0m"
mkfs.ext2 ${SDCARD}2

echo "****************************************"
echo "******  Prepare the  lk.bin      *******"
echo "******        SEU                *******"
echo "******  chinawrj@gmail.com       *******"
echo "****************************************"
/bin/echo -e "\x1b[1;31m"
echo "preparing boot.bin"
/bin/echo -e "\x1b[0m"
dd if=lk.bin of=lk.sd.bin bs=1 count=446
dd if=${SDCARD} bs=512 count=1 |dd of=lk.sd.bin bs=1 skip=446 conv=notrunc oflag=append
dd if=lk.bin of=lk.sd.bin conv=notrunc oflag=append bs=1 skip=512

echo "****************************************"
echo "******   copy files to SD SCARD  *******"
echo "******          SEU              *******"
echo "******     chinawrj@gmail.com    *******"
echo "****************************************"
/bin/echo -e "\x1b[1;31m"
echo "copy lk.bin....."
/bin/echo -e "\x1b[0m"
dd if=lk.sd.bin of=${SDCARD} >> /dev/null
/bin/echo -e "\x1b[1;31m"
echo "copy boot.img...."
/bin/echo -e "\x1b[0m"
dd if=boot.img of=${SDCARD} bs=512 seek=1000 >> /dev/null
/bin/echo -e "\x1b[1;31m"
echo "copy rootfs...."
/bin/echo -e "\x1b[0m"
mkdir vfat -p
mkdir ext2 -p
mkdir ramdisk -p
mount -t vfat ${SDCARD}1 vfat
mount -t ext2 ${SDCARD}2 ext2
mount -o loop ramdisk80m.img ramdisk 
cp ramdisk/* -afpv ext2
cd ext2
ln -s bin/busybox init
/bin/echo -e "\x1b[1;31mUmounting SDCARD...."
echo "This may take 20 seconds.."
sleep 5
cd ..
umount vfat
umount ext2
/bin/echo -e "Done! You can remove your sdcard now!\x1b[0m"
