#ifndef __HAN__
#define	__HAN__

#define U8	unsigned char
#define U16	unsigned short
#define U32	unsigned int

struct fontHeader{
	U8	magic[4];
	U32	Size;
	U8	nSection;
	U8	YSize;
	U16	wCpFlag;
	U8	reserved[4];
};
struct sectionInfo{
	U16	First;
	U16	Last;
	U32	OffAddr;
};
struct charInfo{
	U32	OffAddr : 26;
	U32	Width	: 6;
};
struct fontMap{
	struct	fontHeader	*headerInfo;
	struct	sectionInfo	*sectionInfo;
	struct	charInfo	*charInfo;
	unsigned char		name[16];
	unsigned char 		*priv;
};
struct charMap{
	unsigned char	*utf8;
	int	utf8_length;
	unsigned int height;
	unsigned int charCount;
	U32	unicode[64];
	unsigned char	*bitMap[64];
};

#endif
