#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include "han.h"


#define SIZE_FONT	1024*1024*4

/*Function declare*/
unsigned int unicode4utf8(unsigned char *, int);
static int getcodenum(unsigned char);

/*global varible*/
struct fontMap font16 =
{
	.headerInfo	=	NULL,
	.sectionInfo	=	NULL,
	.charInfo	=	NULL,
	.name		=	"msyh_U16.bin",
};
struct fontMap font24 =
{
	.headerInfo	=	NULL,
	.sectionInfo	=	NULL,
	.charInfo	=	NULL,
	.name		=	"msyh_U24.bin",
};
struct fontMap font32 =
{
	.headerInfo	=	NULL,
	.sectionInfo	=	NULL,
	.charInfo	=	NULL,
	.name		=	"simfang_U32.bin",
};
struct fontMap *font_list[]=
{
	&font16,
	&font24,
	&font32,
	NULL,
};


void printHeader(struct fontHeader *tempPoint)
{
	printf("************Font Header*********************\n");
	printf("Header size:%d\n", (int)sizeof(struct fontHeader));
	//printf("Magic Number:%s\n", tempPoint->magic);
	printf("Size:%d\n", tempPoint->Size);
	printf("Section Number:%d\n", tempPoint->nSection);
	printf("Font reselution:%d\n", tempPoint->YSize);
	printf("Code Flag:%x\n", tempPoint->wCpFlag);
	printf("Reserved bytes:%d\n", *(U32 *)(&(tempPoint->reserved)));
}

void printSection(struct sectionInfo *tempSection)
{
	printf("************Section Header******************\n");
	printf("First Character:%d\n", tempSection->First);
	printf("Last Character:%d\n", tempSection->Last);
	printf("Base offset address:%0x\n", tempSection->OffAddr);
}

void printChar(struct charInfo *tempChar)
{
	printf("************Char Header******************\n");
	printf("Offset font address:%0x\n", tempChar->OffAddr);
	printf("Font width:%d\n", tempChar->Width);
}

void fontDisplay(struct fontMap *font)
{
	int i, j, k, tempB;
	U8 * tempPoint;
	U8 tempDot, tempPos;
	tempB = (font->charInfo->Width)/8;
	for(i = 0; i < font->charInfo->Width; i++)
	{
		for(j = 0; j < tempB; j++)
		{
			tempPoint = (U8 *)(font->headerInfo) + font->charInfo->OffAddr + i*tempB + j;
			tempDot = *tempPoint;
			tempPos = 0x80;
			for(k = 0; k < 8; k++)
			{
				if(tempDot & tempPos)
					printf("*");
				else
					printf(".");
				tempPos = tempPos >> 1;

			}
		}
		printf("%d,%d,%x\n", i, j, (U32)tempPoint);
	}
}

void setFontData(struct fontMap *font, void * ptr)
{
	font->headerInfo = (struct fontHeader *)ptr;
	font->sectionInfo = (struct sectionInfo *)((U8 *) ptr + sizeof(struct fontHeader));
	font->priv = (unsigned char*) ptr;
	printHeader(font->headerInfo);
	printSection(font->sectionInfo);
}

void setChar(struct fontMap *font, U32 UnicodeTemp)
{
	font->charInfo = (struct charInfo *)(font->sectionInfo->OffAddr + (UnicodeTemp - font->sectionInfo->First)*sizeof(struct charInfo) + (U8 *)font->headerInfo);
	printChar(font->charInfo);
	fontDisplay(font);
}

int font_list_init(struct fontMap **fontList)
{
	int fd;
	int j, k;
	int i = 0;
	U32 unicodeChar = 0;
	unsigned char * tempP = NULL;
	struct fontMap *tempFont = NULL;
	unsigned char tempCode[8];
	j = 0;
	while(fontList[j] != NULL)
	{
		tempFont = fontList[j];
		j++;
		printf("font file name:%s\n", tempFont->name);
		fd = open(tempFont->name, O_RDONLY);
		if(fd == -1)
		{
			printf("File open error!\n");
			return -1;
		}
		tempP = malloc(SIZE_FONT);
		if(tempP == NULL)
		{
			printf("No enough memory for Font file:%s!\n", tempFont->name);
			return -2;
		}
		printf("Now test for font file!\n");
		i = read(fd, tempP, SIZE_FONT);
		printf("read %d bytes from %s\n", i, tempFont->name);
		close(fd);
		setFontData(tempFont, tempP);

		fd = open("test.txt", O_RDONLY);
		if(fd == -1)
		{
			printf("File open error!\n");
			return -1;
		}
		read(fd, tempCode, 8);
		close(fd);
		unicodeChar = unicode4utf8(tempCode, getcodenum(*(unsigned char *)tempCode));
		printf("utf8 code:%x,%x,%x,%x,%x,%x,%x,%x, unicode:%x\n", *tempCode, *(tempCode+1), *(tempCode+2), *(tempCode+3), *(tempCode+4), *(tempCode+5), *(tempCode+6), *(tempCode+7), unicodeChar);
		printf("size of charInfo is:%d\n", (int)sizeof(struct charInfo));
		setChar(tempFont, unicodeChar);
	}
}
int convert2bitmap(struct charMap *tempMap, struct fontMap *tempFont)
{	
	unsigned char *i = tempMap->utf8;
	int j, codeCount, utf8length;
	struct charInfo *tempChar;
	if(i == NULL)
	{
		printf("Error! empty utf8 string\n");
		return -1;
	}

	/*convert UTF-8 code*/
	codeCount = 0;
	utf8length = 0;
	while(utf8length < tempMap->utf8_length)
	{
		j = getcodenum(*(unsigned int *)i);
		if(j)
			utf8length += 3; /*it maybe 3, so we just add to it*/
			/*FIXME:now we only suport the utf8 code whose length is 3*/
		else
			break;

		tempMap->unicode[codeCount] = unicode4utf8(i, j);
		i = i + j;/*update string pointer*/
		codeCount = codeCount + 1;/*update total number for char*/
	}

	/*convert bit_map*/
	for(j = 0; j < codeCount; j++)
	{
		tempChar = (struct charInfo *)(tempFont->sectionInfo->OffAddr + (tempMap->unicode[j] - tempFont->sectionInfo->First)*sizeof(struct charInfo) + (U8 *)tempFont->headerInfo);
		tempMap->bitMap[j] = (U8 *)(tempFont->headerInfo) + tempChar->OffAddr;
	}
	/*Set bit map height*/
	tempMap->height = tempFont->headerInfo->YSize;
	tempMap->charCount = codeCount;
	printf("convert complete! the total number of chars is %d\n", tempMap->charCount);
	return 0;
}

void displayBitMap(struct charMap *tempMap)
{
	int i, j, k, l, tempB;
	U8 * tempPoint;
	U8 tempDot, tempPos;
	tempB = (tempMap->height)/8;
	printf("the total number of chars to be displayed is %d\n", tempMap->charCount);
	for(l = 0; l < tempMap->charCount; l++)
	{
		for(i = 0; i < tempMap->height; i++)
		{
			for(j = 0; j < tempB; j++)
			{
				tempPoint = (U8 *)(tempMap->bitMap[l]) + i*tempB + j;
				tempDot = *tempPoint;
				tempPos = 0x80;
				for(k = 0; k < 8; k++)
				{
					if(tempDot & tempPos)
						printf("*");
					else
						printf(".");
					tempPos = tempPos >> 1;

				}
			}
			printf("%d,%d,%x\n", i, j, (U32)tempPoint);
		}
	}

}


int main(void)
{
	int fd;
	struct charMap testCharMap;
	font_list_init(font_list);
	fd = open("test.txt", O_RDONLY);
	if(fd == -1)
	{
		printf("Open test.txt error\n");
		return -1;
	}
	testCharMap.utf8 = malloc(1024);
	if(testCharMap.utf8 == NULL)
	{
		printf("No enough memory for string cache\n");
		return -1;
	}
	testCharMap.utf8_length = read(fd, testCharMap.utf8, 1024);
	close(fd);
	printf("Read %d bytes from test.txt\n", testCharMap.utf8_length);
	convert2bitmap(&testCharMap, &font24);
	displayBitMap(&testCharMap);
	return 1;
}


unsigned int unicode4utf8(unsigned char *utf8, int codeNum)
{
	unsigned int tempUnicode, tempI;
	tempUnicode = 0;
	switch(codeNum){
		case 0:
			return (U32)(*utf8);

		case 3:
			tempUnicode = ((U32)((*utf8) & 0x0f) << 12) | (U32)((*(utf8 + 1)) & 0x3f) << 6 | (U32)((*(utf8 + 2)) & 0x3f);
			break;
		default:
			printf("BUG:We only support 1 byte and 3 bytes code length for UTF-8\n");
			return -1;
	}
	return tempUnicode;
}

static int getcodenum(unsigned char utfHead)
{
	unsigned char tempPos = 0x80;
	int charNum = 0;
	while(utfHead & tempPos)
	{
		charNum++;
		tempPos = (tempPos >> 1);
	}
	printf("code length:%d\n", charNum);
	return charNum;
}
