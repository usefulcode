#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

typedef struct {
	int x;
	int y;
	int z;
	int delay_time;
} gsensor_accel_t;

enum DIRECTION{
	DIRECTION_UP = 0,
	DIRECTION_DN,
	DIRECTION_LF,
	DIRECTION_RT,
	DIRECTION_FT,
	DIRECTION_BK,
	DIRECTION_MAX
};

#define READ_SIZE sizeof(gsensor_accel_t)
#define DEV_PATH "/dev/gsensor"

int getXYZ(gsensor_accel_t *bufData)
{
	int fd;
	int i;
	memset(bufData, 0, READ_SIZE);
	fd = open(DEV_PATH, O_RDONLY);
	if(fd == -1)
	{
		printf("open ##DEV_PATH failed!\n");
		return -1;
	}
	i = read(fd, bufData, READ_SIZE);
	close(fd);
	if(i == 0)
		i = READ_SIZE; /*See the programmer's manual*/
	printf("Read %d bytes from DEV_PATH, Intended size is %d\n", i, READ_SIZE);
	printf("x = %d\n" , bufData->x);
	printf("y = %d\n" , bufData->y);
	printf("z = %d\n" , bufData->z);
	printf("g = %d\n" , bufData->x*bufData->x + bufData->y*bufData->y + bufData->z*bufData->z);
	return 0;
}

void msleep(int i){
	usleep(1000*i);
}

enum DIRECTION isDirection(int x, int y, int z)
{
	if((x*x + y*y < 32) && z*z > 225)
	{
		if(z > 0)
			return DIRECTION_UP;
		else
			return DIRECTION_DN;
	}

	if((x*x + z*z < 32) && y*y > 225)
	{
		if(y > 0)
			return DIRECTION_RT;
		else
			return DIRECTION_LF;
	}

	if((y*y + z*z < 32) && x*x > 225)
	{
		if(x > 0)
			return DIRECTION_FT;
		else
			return DIRECTION_BK;
	}

	return DIRECTION_MAX;
}

int waitDirection(enum DIRECTION tempDir, int max_Time)
{
	gsensor_accel_t tempData;
	int i;
	for(i = 0; i< max_Time; i++)
	{
		getXYZ(&tempData);
		if(isDirection(tempData.x, tempData.y, tempData.z) == tempDir)
			return 1;
		else
			msleep(100);
	}
	return 1;
}

void usage()
{
	printf("usage:\n");
	printf("*******************************************\n");
	printf("**      gsensor DIRECTION                **\n");
	printf("**      DIRECTION: up down               **\n");
	printf("**                 left right            **\n");
	printf("**                front back             **\n");
	printf("*******************************************\n");
}

int main(int argc, char *argv[]){
	unsigned int i;
	unsigned int foundDir;
	unsigned char *dirChar[DIRECTION_MAX]={
		"up",
		"down",
		"left",
		"right",
		"front",
		"back"
	};

	if(argc == 1)
	{
		usage();
		return 0;
	}

	for(i = 0; i < DIRECTION_MAX; i++)
	{
		if(!strcmp(dirChar[i], argv[1]))
			break;
	}
	foundDir = waitDirection((enum DIRECTION)i, 200);
	if(foundDir)
		printf("Direction Detected!\n");
	else
		printf("Time out occured! please check!\n");
}
